import torch
import torchvision
import torch.utils.data
from torch.autograd import Variable
import torch.nn.functional as F
import matplotlib.pyplot as plt

import pickle

from models import LeNet, load_model
from utils import transform

###############
# Parameters
###############
images_dir = 'WCT_Def/images'
model_dir = 'WCT_Def/trained_models/lenet_mnist.pth'
image_size = 28
epsilon = 0.3
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
lr = 0.001

###############
# Dataset
###############
test_loader = torch.utils.data.DataLoader(
     torchvision.datasets.MNIST('~/AI/Datasets/mnist/data/', train=False, download=True, transform=torchvision.transforms.Compose([
        torchvision.transforms.Resize(image_size),
        torchvision.transforms.ToTensor(),
        ])),
    batch_size=1, shuffle=False)

#test_loader = torch.utils.data.DataLoader(
#    torchvision.datasets.ImageFolder(root=images_dir ,transform=torchvision.transforms.Compose([
#        torchvision.transforms.Resize(image_size),
#        torchvision.transforms.ToTensor()
#        ])),
#    batch_size=1
#)

###############
# Models
###############
lenet = LeNet(in_channels=1).to(device)
optimizer = torch.optim.Adam(lenet.parameters(), lr=lr)
lenet,_,_ = load_model(lenet, optimizer=optimizer, root=model_dir)


with open('WCT_Def/data/advs_0.3.pickle','rb') as pickle_file:
    data = pickle.load(pickle_file)

correct = 0
csF = torch.Tensor()
csF = Variable(csF)
for i,(real_img, real_label, pert_img, pert_label) in enumerate(data):
    real_img = torch.Tensor(real_img).to(device)
    pert_img = torch.Tensor(pert_img).to(device)

    cf1 = lenet.encode1(pert_img)
    sf1 = lenet.encode1(real_img)
    cf1 = cf1.data.cpu().squeeze(0)
    sf1 = sf1.data.cpu().squeeze(0)
    csF1 = transform(cf1, sf1, csF, alpha=1).to(device)
    out1 = lenet.decode1(csF1)

    cf2 = lenet.encode2(pert_img)
    sf2 = lenet.encode2(real_img)
    cf2 = cf2.data.cpu().squeeze(0)
    sf2 = sf2.data.cpu().squeeze(0)
    csF2 = transform(cf2, sf2, csF, alpha=1).to(device)
    out2 = lenet.decode2(csF2)

    #out = lenet(pert_img)


    pred = out2.max(1, keepdim=True)[1]

    if pred.item() == real_label:
        correct += 1

print(correct / len(data))




