import torch
import torch.nn as nn
import torch.nn.functional as F

#################
# Autoencoder
#################
class encoder(nn.Module):
    def __init__(self, in_channels=1, ngf=32):
        super(encoder, self).__init__()
        self.l1 = nn.Sequential(
            nn.ReflectionPad2d(padding=3),
            nn.Conv2d(in_channels, ngf, 7),
            nn.ReLU()
        )
        self.l2 = nn.Sequential(
            nn.Conv2d(ngf, 2 * ngf, 3),
            nn.ReLU()
        )
        self.l3 = nn.Sequential(
            nn.Conv2d(2 * ngf, 4 * ngf, 3),
            nn.ReLU()
        )
        self.l4 = nn.Sequential(
            nn.Conv2d(4 * ngf, 8 * ngf, 3),
            nn.ReLU()
        )
        # Layers with no downsampling
        self.l5 = nn.Sequential(
            nn.ReflectionPad2d(1),
            nn.Conv2d(8*ngf, 8*ngf, 3),
            nn.ReLU()
        )
        self.l6 = nn.Sequential(
            nn.ReflectionPad2d(1),
            nn.Conv2d(8 * ngf, 8 * ngf, 3),
            nn.ReLU()
        )

    def forward(self, input):
        out = self.l1(input)
        out = self.l2(out)
        out = self.l3(out)
        out = self.l4(out)
        out = self.l5(out)
        out = self.l6(out)
        return out

class decoder(nn.Module):
    def __init__(self, out_channels=1, ngf=32):
        super(decoder, self).__init__()
        # Layers with no upsampling
        self.l1 = nn.Sequential(
            nn.ReflectionPad2d(1),
            nn.Conv2d(8 * ngf, 8 * ngf, 3),
            nn.ReLU()
        )
        self.l2 = nn.Sequential(
            nn.ReflectionPad2d(1),
            nn.Conv2d(8 * ngf, 8 * ngf, 3),
            nn.ReLU()
        )

        self.l3 = nn.Sequential(
            nn.ConvTranspose2d(8*ngf, 4*ngf, 3),
            nn.ReLU()
        )
        self.l4 = nn.Sequential(
            nn.ConvTranspose2d(4*ngf, 2*ngf, 3),
            nn.ReLU()
        )
        self.l5 = nn.Sequential(
            nn.ConvTranspose2d(2 * ngf,  ngf, 3),
            nn.ReLU()
        )
        self.l6 = nn.Sequential(
            nn.ConvTranspose2d(ngf, out_channels, 7, padding=3),
            nn.Tanh()
        )

    def forward(self, input):
        out = self.l1(input)
        out = self.l2(out)
        out = self.l3(out)
        out = self.l4(out)
        out = self.l5(out)
        out = self.l6(out)
        return out

###################
# Classifiers
###################
class LeNet(nn.Module):
    def __init__(self, in_channels=1):
        super(LeNet, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, 10)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)

    def encode1(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        return x

    def decode1(self, x):
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)

    def encode2(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        return x

    def decode2(self, x):
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)


####################
# Helper Functions
####################
def save_model(model ,epoch, optim , filename):
    torch.save({
        'epoch': epoch,
        'state_dict': model.state_dict(),
        'optimizer': optim.state_dict(),
    }, filename)

def load_model(model, optimizer, root):
    checkpoint = torch.load(root)
    epoch = checkpoint['epoch']
    model.load_state_dict(checkpoint['state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer'])
    return model, optimizer, epoch
